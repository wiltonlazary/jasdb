---
layout: about
permalink: /about/
title: About Oberasoftware
weight: 1
modified: 14-07-2014
comments: false
---

## About Us
Obera Software is a fresh new startup looking to add something of value to the NoSQL community. We are guided by the principle of simplicity which gives its name to our first product, JasDB (Just A Simple DB). We apply simplicity to everything we do, from our website, to the deep inner workings of our storage solution and you will experience the difference when you use JasDB. Thats why you won’t find a lot of waste in our site or in our product. Just the bare essentials that add real value.

### Our Founder
Meet our founder, Renze De Vries. Principal Java developer with almost 10 years of experience in Java development and NoSQL expert. Renze has been working hard over the past years to make his dream of creating the next generation in storage a reality. Besides being a creative visionary, Renze enjoys gaming and taking his mini choppers out for a spin on windless days.

### Contact Us
Are you interested in Obera Software and JasDB? Would you like to partner with us or build a product ontop of JasDB? If so, mail us at info@oberasoftware.com, we would love to hear from you.

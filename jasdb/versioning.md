---
layout: page
permalink: /jasdb/versioning/
title: JasDB Versioning
group: JasDB
weight: 5
tagline: JASDB
tags: [jasdb, nosql, document, database, java, fast]
modified: 14-7-2014
comments: false
---

## Release versioning
The release versioning uses the following schema:

major.minor.service.patch.

### Major release
A Major release is intended for large changes in JasDB. These might be breaking changes, but will most often contain multiple new features, enabling many new options for the end user.

### Minor release
A Minor release can contain minor API changes and new API’s being introduced. A minor release can also result in deprecation of old API’s. New features can also be introduced in a minor release as long as backwards compatibility is maintained.

### Service release
A service release is intended to combine a number of fixes in JasDB. It can also contain extensions to the API to augment existing features.

### Patch release
A patch release contains fixes for one or multiple bugs, intended to improve product stability. This type of release will not break or change any API’s.

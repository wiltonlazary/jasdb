---
layout: page
permalink: /jasdb/nosql/
title: What is NoSQL
tagline: JASDB
group: JasDB
weight: 3
tags: [jasdb, nosql, document, database, java, fast]
modified: 14-7-2014
comments: false
---

## What is NoSQL?
NoSQL databases have existed since 1998 in various forms, so the concept in itself is not new. The term NoSQL has also existed since 1998 and several incarnations of NoSQL databases have been invented during past years in products like Lotus Notes and Facebook. These were solutions engineered in response to problems experienced by the software developers regarding scaling and quick retrieval of data, but still NoSQL was not a general mindset when these solutions were invented. These were solutions created to meet the urgent need of the product, but the category of problems that was being addressed by these solutions had not been conceived of as yet.

The recent popularity and affordability of the internet and smart-phones have increased the scale of users generating data. These masses of data need to be stored and accessed on fast enough for today’s users to stay engaged. Conventional data storage mechanisms were not equipped to deal with all this activity.That’s where NoSQL comes in.

NoSQL is a step away from conventional database design. The old-style relational databases organized data into tables containing rows and columns, with specific columns selected to relate data between tables. NoSQL databases are actually a grouping of several different database design approaches into one category. Each different type of NoSQL database has its own applications and its own desgn trade-offs. Its best to consider these before choosing a specific type of NoSQL database.

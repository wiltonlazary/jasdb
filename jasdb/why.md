---
layout: page
permalink: /jasdb/why/
title: Why JasDB
tagline: JASDB
group: JasDB
weight: 1
tags: [jasdb, nosql, document, database, java, fast]
modified: 14-7-2014
comments: false
---

## Why use JasDB?
JasDB is pretty unique in that it’s a Java-based NoSQL database, which can run inside your Java process.

This makes JasDB useful for quick prototyping. Further it can be convenient to run your db in Java if you are developing in Java. Since JasDB has been made open source, you can run the db from local code and debug through the code, speeding up the development process. The in-memory feature is also useful to support quick unit testing, or apps that require a queryable cache database. Indexes and data can be configured to run completely in-memory or to write to disk for your convenience.

JasDB also runs on Android, which means that you can store unstructured data from your mobile applications conveniently in JSON format. This gives you as a developer more flexibility and lower maintenance during mobile application development.

We have a great vision for the future of JasDB. Stick with us for more great features in the future.

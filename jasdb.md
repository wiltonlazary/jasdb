---
layout: page
permalink: /jasdb/
group: JasDB
weight: 1
title: About JasDB
tagline: JASDB
tags: [jasdb, nosql, document, database, java, fast]
modified: 14-7-2014
comments: false
---

### Description
JasDB is a NoSQL database using a document-based storage mechanism. We developed JasDB to provide an alternative to current document-based implementations out there, to add something new to the industry and give users more choices. We want to put in our two-cent’s-worth to help the industry into a new stage of maturity. So what does this have to do with you, dear user? Well, we at Obera believe that software should be based on real -life user needs, and we are very interested in your needs. If you invest your time and effort into using JasDB, your return will be a product that grows to meet your needs. We are interested in your feedback, so please do not hesitate to contact us with your ideas and requirements as we walk this journey together.

JasDB was designed with easy of use and minimal configuration in mind. JasDB can be installed and configured in almost no time at all. Use JasDB as an embedded DB inside your JVM or you can use it to store your unstructured data in JSON format. If you want to scale JasDB, simply run and use it through the REST webservice.

### Features of JasDB:

* Run as embedded DB inside the JVM
* JSON Document storage
* REST interface
* Runs on Android
* Complete Querying functionality
* High concurrency environment
* Stackable components allow great flexibility
* Extensible
* User security and granting model



But don’t let us bore you with the details, and don’t take our word for it! Why not try JasDB and see for yourself how easy and comfortable it is to use. Installation is a cinch and you can be up and running within minutes, because its a simple product and fun product. Just hop to our download page and you can see for yourself.

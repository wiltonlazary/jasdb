---
layout: page
permalink: /support/
title: JasDB Support options
tagline: JASDB
weight: 1
tags: [jasdb, nosql, document, database, java, fast]
modified: 14-7-2014
comments: false
---

### Support
We have free support available for the open source version of JasDB, file any ticket using our issue tracker.

When you have found a bug in JasDB or you would like to register an enhancement request, you can report it in our issue tracker.

[Issue tracker](https://bitbucket.org/oberasoftware/jasdb_open/issues)
